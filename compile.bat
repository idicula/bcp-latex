@echo off
SETLOCAL EnableExtensions EnableDelayedExpansion

rem Compile music snippets with lilypond into an output folder.
rem We do this because the output files are unknown, so this is a hack
rem that lets us get a list of the output files.
lilypond-book --output=out --pdf %1.lytex 

rem For paths in the lytex file to not break, we need to move the output
rem back into the original directory.
rem First, we get the list of files in the output dir:
if exist out_files.txt (del out_files.txt)
for %%f in ("out\*") do (echo %%f >> out_files.txt)
set outfiles=
for /f "delims=" %%i in (out_files.txt) do (set outfiles=!outfiles!, %%i)

rem Then the list of subdirs, there may not be any:
if exist out_dirs.txt (del out_dirs.txt)
for /D %%s in ("out\*") do (echo %%s >> out_dirs.txt)
set outdirs=
for /f "delims=" %%i in (out_dirs.txt) do (set outdirs=!outdirs!, %%i)

echo %outfiles%
echo %outdirs%

rem REM Now copy the files and subdirs to the original dir:
for %%i in (%outfiles%) do (move %%i %%~pi..)
for %%i in (%outdirs%) do (move %%i %%~pi..)

rem REM We can now safely compile the latex without breaking the paths:
xelatex %1

rem REM But we need to clean up after ourselves.
rem REM First, delete the original output dir:
rmdir out /s /q

rem REM Then, delete all the files and subdirs we copied.
rem REM We need to remove the output/ prefix:
for %%i in (%outfiles%) do (del %%~nxi)
for %%i in (%outdirs%) do (rmdir %%~nxi /s /q)

rem Finally, delete the temp txt files we wrote
if exist out_files.txt (del out_files.txt)
if exist out_dirs.txt (del out_dirs.txt)


rem cd out/
rem xelatex %1
rem move %1.pdf ../%1.pdf
rem cd ..
rem rmdir out /s /q

